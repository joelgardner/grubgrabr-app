//
//  DashboardViewController.m
//  Grubgrabr
//
//  Created by Joel Gardner on 11/27/13.
//  Copyright (c) 2013 Joel Gardner. All rights reserved.
//

#import "DashboardViewController.h"
#import "ADVTheme.h"
#import "DataService.h"
#import "TransactionCell.h"
#import "Transaction.h"
#import "Business.h"


@interface DashboardViewController ()
@property NSArray *transactions;
@end

@implementation DashboardViewController
@synthesize transactions;
@synthesize tableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
       
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
   // load custom theme
   [ADVThemeManager customizeView:self.view];
   //[ADVThemeManager customizeTableView:self.tableView];
   
   // get data
   self.transactions = [[DataService shared] transactions];
   
   // set title
   self.navigationItem.title = @"Grubgrabr";
   
   //self.tableView.backgroundView.backgroundColor = [UIColor clearColor];
   self.tableView.backgroundColor = [UIColor clearColor];
   self.navigationController.navigationBar.barTintColor = [UIColor orangeColor];
   //self.tabBarController.tabBar.backgroundImage = [[ADVThemeManager sharedTheme] tabBarBackground];
   //self.tabBarController.tabBar.position NGTabBarPositionBottom

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle {
   return UIStatusBarStyleLightContent;
}

#pragma mark UITableViewDelegate & UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
   return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   return [self.transactions count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
   return 295.0f;
}

- (UITableViewCell *)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   TransactionCell *c = [_tableView dequeueReusableCellWithIdentifier:@"TransactionCell"];
   if (!c) {
      c = [[TransactionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TransactionCell"];
   }
   Transaction *t = [self.transactions objectAtIndex:indexPath.row];
   
   c.businessNameLabel.text = ((Business *)t.business).name;
   c.descriptionLabel.text = @"$34.70 spent 1 hour ago";
   c.rewardAmountLabel.text = @"34.12";
   c.progressView.progress = 3.0f / 5.0f;
   c.businessImage.image = [UIImage imageNamed:@"eb2.png"];
   
   c.backgroundColor = [UIColor clearColor];
   //c.backgroundView.backgroundColor = [UIColor clearColor];
   return c;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
   return 10.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
   return [UIView new];
}

@end
