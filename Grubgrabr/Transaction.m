//
//  Transaction.m
//  Grubgrabr
//
//  Created by Joel Gardner on 11/29/13.
//  Copyright (c) 2013 Joel Gardner. All rights reserved.
//

#import "Transaction.h"


@implementation Transaction

@dynamic amount;
@dynamic percentBonus;
@dynamic business;

@end
