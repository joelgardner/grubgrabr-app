//
//  DashboardViewController.h
//  Grubgrabr
//
//  Created by Joel Gardner on 11/27/13.
//  Copyright (c) 2013 Joel Gardner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NGTabBarController.h"

@interface DashboardViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet NGTabBar *tabBar;
@end
