//
//  TransactionCell.h
//  Grubgrabr
//
//  Created by Joel Gardner on 11/29/13.
//  Copyright (c) 2013 Joel Gardner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADVProgressBar.h"

@interface TransactionCell : UITableViewCell
@property (nonatomic, strong) UILabel *businessNameLabel;
@property (nonatomic, strong) UILabel *descriptionLabel;
@property (nonatomic, strong) UILabel *rewardAmountLabel;
@property (nonatomic, strong) ADVProgressBar *progressView;
@property (nonatomic, strong) UIImageView *businessImage;
@end
