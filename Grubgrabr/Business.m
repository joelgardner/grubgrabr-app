//
//  Business.m
//  Grubgrabr
//
//  Created by Joel Gardner on 11/29/13.
//  Copyright (c) 2013 Joel Gardner. All rights reserved.
//

#import "Business.h"


@implementation Business

@dynamic name;
@dynamic businessId;
@dynamic percentBase;

@end
