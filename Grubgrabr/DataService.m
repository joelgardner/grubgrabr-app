//
//  DataService.m
//  Grubgrabr
//
//  Created by Joel Gardner on 11/29/13.
//  Copyright (c) 2013 Joel Gardner. All rights reserved.
//

#import "DataService.h"
#import "AppDelegate.h"

@implementation DataService

static DataService *_sharedInstance;
+ (DataService *)shared {
   if (!_sharedInstance) {
      _sharedInstance = [[DataService alloc] init];
   }
   return _sharedInstance;
}

- (NSArray *)transactions {
   //return [NSArray new];
   
   NSManagedObjectContext *ctx = [[AppDelegate sharedDelegate] managedObjectContext];
   NSManagedObjectModel *mdl = [[ctx persistentStoreCoordinator] managedObjectModel];
   
   NSEntityDescription *businessEntity = [[mdl entitiesByName] objectForKey:@"Business"];
   Business *b1 = (Business *)[[NSManagedObject alloc] initWithEntity:businessEntity insertIntoManagedObjectContext:ctx];
   b1.name = @"Bill's Fajita Warehouse";
   b1.percentBase = [NSNumber numberWithInt:10];
   
   Business *b2 = (Business *)[[NSManagedObject alloc] initWithEntity:businessEntity insertIntoManagedObjectContext:ctx];
   b2.name = @"Joel's Chicken & Wafflez";
   b2.percentBase = [NSNumber numberWithInt:8];
   
   NSEntityDescription *TransactionEntity = [[mdl entitiesByName] objectForKey:@"Transaction"];
   Transaction *t1 = (Transaction *)[[NSManagedObject alloc] initWithEntity:TransactionEntity insertIntoManagedObjectContext:ctx];
   t1.percentBonus = [NSNumber numberWithInt:2];
   t1.amount = [NSNumber numberWithDouble:12.34f];
   t1.business = b1;
   
   Transaction *t2 = (Transaction *)[[NSManagedObject alloc] initWithEntity:TransactionEntity insertIntoManagedObjectContext:ctx];
   t2.percentBonus = [NSNumber numberWithInt:3];
   t2.amount = [NSNumber numberWithDouble:9.98f];
   t2.business = b2;
   
   return [NSArray arrayWithObjects:t1, t2, nil];
}
@end
