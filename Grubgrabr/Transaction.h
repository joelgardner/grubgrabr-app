//
//  Transaction.h
//  Grubgrabr
//
//  Created by Joel Gardner on 11/29/13.
//  Copyright (c) 2013 Joel Gardner. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Transaction : NSManagedObject

@property (nonatomic, retain) NSNumber * amount;
@property (nonatomic, retain) NSNumber * percentBonus;
@property (nonatomic, retain) NSManagedObject *business;

@end
