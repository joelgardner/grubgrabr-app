//
//  DataService.h
//  Grubgrabr
//
//  Created by Joel Gardner on 11/29/13.
//  Copyright (c) 2013 Joel Gardner. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Transaction.h"
#import "Business.h"

@interface DataService : NSObject

+ (DataService *)shared;
- (NSArray *)transactions;

@end
