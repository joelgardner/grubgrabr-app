//
//  LoginViewController.m
//  Grubgrabr
//
//  Created by Joel Gardner on 11/27/13.
//  Copyright (c) 2013 Joel Gardner. All rights reserved.
//

#import "LoginViewController.h"
#import "ADVTheme.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
   [ADVThemeManager customizeView:self.view];
   
   UIFont *f = self.username.font;
   NSLog(@"%@", f.fontDescriptor);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
   if (textField == self.username) {
      [self.password becomeFirstResponder];
   }
   else {
      [textField resignFirstResponder];
   }
   
   return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
   return YES;
}

#pragma mark Events/IBActions
- (IBAction)loginButtonTapped:(id)sender {
   
}

@end
