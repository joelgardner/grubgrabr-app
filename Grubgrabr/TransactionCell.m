//
//  TransactionCell.m
//  Grubgrabr
//
//  Created by Joel Gardner on 11/29/13.
//  Copyright (c) 2013 Joel Gardner. All rights reserved.
//

#import "TransactionCell.h"
#import <QuartzCore/QuartzCore.h>
@interface TransactionCell ()
@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIView *bgImageView;
@property (nonatomic, strong) UIView *header;
@property (nonatomic, strong) UILabel *toBalanceLabel;
@property (nonatomic, strong) UIView *divider;
@property (nonatomic, strong) UIView *footer;
@property (nonatomic, strong) UILabel *reviewIcon;
@end

@implementation TransactionCell
@synthesize businessNameLabel;
@synthesize descriptionLabel;
@synthesize rewardAmountLabel;
@synthesize progressView;
@synthesize businessImage;
@synthesize bgView;
@synthesize bgImageView;
@synthesize divider;
@synthesize footer;
@synthesize header;
@synthesize reviewIcon;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
       // business image setup
       self.businessImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"eb.png"]];
       self.businessImage.layer.cornerRadius = 4.0f;
       self.businessImage.layer.masksToBounds = YES;
       self.businessImage.translatesAutoresizingMaskIntoConstraints = NO;
       
       self.businessNameLabel = [[UILabel alloc] init];
       self.businessNameLabel.translatesAutoresizingMaskIntoConstraints = NO;
       self.businessNameLabel.font = [UIFont fontWithName:@"AvenirNext-DemiBold" size:18.0f];
       self.businessNameLabel.textColor = [UIColor whiteColor];

       
       self.descriptionLabel = [[UILabel alloc] init];
       self.descriptionLabel.translatesAutoresizingMaskIntoConstraints = NO;
       self.descriptionLabel.font = [UIFont fontWithName:@"AvenirNext-DemiBoldItalic" size:10.0f];
       self.descriptionLabel.textColor = [UIColor lightTextColor];
       
       self.progressView = [[ADVProgressBar alloc] initWithFrame:CGRectMake(0, 0, 180, 9)];
       self.progressView.translatesAutoresizingMaskIntoConstraints = NO;
       
       self.rewardAmountLabel = [[UILabel alloc] init];
       self.rewardAmountLabel.translatesAutoresizingMaskIntoConstraints = NO;
       self.rewardAmountLabel.textColor = [UIColor orangeColor];
       self.rewardAmountLabel.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:36.0f];
       
       self.bgView = [[UIView alloc] init];
       self.bgView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
       //self.bgView.layer.cornerRadius = 6.0f;
       //self.bgView.layer.masksToBounds = YES;
       
       self.bgImageView = [[UIImageView alloc] init];
       self.bgImageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
       //self.bgImageView.layer.cornerRadius = 6.0f;
       //self.bgImageView.layer.masksToBounds = YES;
       
       self.toBalanceLabel = [[UILabel alloc] init];
       self.toBalanceLabel.text = @"available";
       self.toBalanceLabel.textColor = [UIColor lightGrayColor];
       self.toBalanceLabel.font = [UIFont fontWithName:@"AvenirNext-DemiBoldItalic" size:12.0f];
       self.toBalanceLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
       self.divider = [[UIView alloc] init];
       self.divider.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
       
       self.header = [[UIView alloc] init];
       self.header.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
       
       self.footer = [[UIView alloc] init];
       self.footer.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
       
       self.reviewIcon = [[UILabel alloc] init];
       self.reviewIcon.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
   [super layoutSubviews];
   
   // autolayout
   NSDictionary *viewsDict = @{@"businessImage": self.businessImage,
                               @"businessNameLabel": self.businessNameLabel,
                               @"descriptionLabel": self.descriptionLabel,
                               @"progressView": self.progressView,
                               @"rewardAmountLabel": self.rewardAmountLabel,
                               @"toBalanceLabel": self.toBalanceLabel,
                               @"reviewIcon": self.reviewIcon };
   
   // background
   self.bgImageView.frame = CGRectMake(10, 5, self.bounds.size.width - 20, self.bounds.size.height - 10);
   self.bgImageView.layer.contents = (id)[UIImage imageNamed:@"ebbg.png"].CGImage;
   self.bgImageView.layer.opacity = 1.0f;
   [self.contentView addSubview:self.bgImageView];
   
   self.bgView.frame = CGRectMake(10, 5, self.bounds.size.width - 20, self.bounds.size.height - 10);
   self.bgView.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.7f];
   [self.contentView addSubview:self.bgView];

   // header view
   self.header.frame = CGRectMake(0, 0, self.bounds.size.width - 20, 60);
   self.header.backgroundColor = [UIColor colorWithWhite:0.8f alpha:0.8f];
   [self.bgView addSubview:self.header];
   
   // image
   /*
   [self.bgView addSubview:self.businessImage];
   [self.bgView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-5-[businessImage]" options:0 metrics:nil views:viewsDict]];
   [self.bgView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[businessImage]" options:0 metrics:nil views:viewsDict]];
   */
    
   // business label
   [self.bgView addSubview:self.businessNameLabel];
   [self.bgView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-5-[businessNameLabel]" options:0 metrics:nil views:viewsDict]];
   [self.bgView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-4-[businessNameLabel]" options:0 metrics:nil views:viewsDict]];
   
   // desc label
   [self.bgView addSubview:self.descriptionLabel];
   [self.bgView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-6-[descriptionLabel]" options:0 metrics:nil views:viewsDict]];
   [self.bgView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[businessNameLabel]-(-4)-[descriptionLabel]" options:0 metrics:nil views:viewsDict]];
   
   // progress view
   [self.bgView addSubview:self.progressView];
   [self.bgView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-6-[progressView]-10-|" options:0 metrics:nil views:viewsDict]];
   [self.bgView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[descriptionLabel]-2-[progressView]-10-|" options:0 metrics:nil views:viewsDict]];
   
   // reward amount label
   [self.bgView addSubview:self.rewardAmountLabel];
   [self.bgView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[rewardAmountLabel]-14-|" options:0 metrics:nil views:viewsDict]];
   [self.bgView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-4-[rewardAmountLabel]" options:0 metrics:nil views:viewsDict]];
   
   // "to balance" label
   [self.bgView addSubview:self.toBalanceLabel];
   [self.bgView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[toBalanceLabel]-20-|" options:0 metrics:nil views:viewsDict]];
   [self.bgView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[rewardAmountLabel]-(-6)-[toBalanceLabel]" options:0 metrics:nil views:viewsDict]];
   
   // divider view
   self.divider.frame = CGRectMake(0, 60, self.bounds.size.width - 20, 1);
   self.divider.backgroundColor = [UIColor colorWithRed:0.92f green:0.92f blue:0.92f alpha:1.00f];
   //[self.bgView addSubview:self.divider];
   
   // footer view
   self.footer.frame = CGRectMake(0, self.bounds.size.height - 42 - 10, self.bounds.size.width - 20, 42);
   self.footer.backgroundColor = [UIColor colorWithWhite:0.92f alpha:0.7f];
   [self.bgView addSubview:self.footer];
   
   // review icon
   [self.footer addSubview:self.reviewIcon];
   [self.footer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-1-[reviewIcon]" options:0 metrics:nil views:viewsDict]];
   [self.footer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-1-[reviewIcon]" options:0 metrics:nil views:viewsDict]];
}

@end
