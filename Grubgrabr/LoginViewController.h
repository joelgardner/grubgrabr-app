//
//  LoginViewController.h
//  Grubgrabr
//
//  Created by Joel Gardner on 11/27/13.
//  Copyright (c) 2013 Joel Gardner. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UITextField *username;
@property (nonatomic, strong) IBOutlet UITextField *password;

- (IBAction)loginButtonTapped:(id)sender;

@end
