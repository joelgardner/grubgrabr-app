//
//  AppDelegate.h
//  Grubgrabr
//
//  Created by Joel Gardner on 11/25/13.
//  Copyright (c) 2013 Joel Gardner. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (AppDelegate*)sharedDelegate;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
